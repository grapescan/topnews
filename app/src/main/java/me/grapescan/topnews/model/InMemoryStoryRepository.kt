package me.grapescan.topnews.model

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.grapescan.topnews.model.api.ApiClient

/**
 * @author Dmitry Soldatov
 */
class InMemoryStoryRepository(private val apiClient: ApiClient) : StoryRepository {

    override val stories: Subject<Result<List<Story>>> = BehaviorSubject.create()

    override fun refreshStories(): Completable {
        return apiClient.getTopStories()
                .map { it.subList(0, 10) } //TODO: remove this
                .subscribeOn(Schedulers.io())
                .toObservable()
                .flatMapIterable { it }
                .flatMapSingle { apiClient.getItem(it) }
                .map {
                    Story(it.id, it.author ?: "", it.time ?: 0L, it.title ?: "", it.url
                            ?: "", it.descendantsCount ?: 0)
                }
                .toList()
                .map { Result.value(it) }
                .onErrorResumeNext { error -> Single.just(Result.error(error)) }
                .doOnSuccess(stories::onNext)
                .toCompletable()
    }

    override fun getComments(storyId: Long): Single<Result<List<Comment>>> {
        return apiClient.getItem(storyId)
                .subscribeOn(Schedulers.io())
                .map { it.commentIds }
                .toObservable()
                .flatMapIterable { it }
                .flatMapSingle { apiClient.getItem(it) }
                .map {
                    Comment(it.id, 1, it.author ?: "", it.time ?: 0L, it.text ?: "", it.commentIds
                            ?: emptyList())
                }
                .toList()
                .map { Result.value(it) }
                .onErrorResumeNext { error -> Single.just(Result.error(error)) }

    }

    override fun getStory(storyId: Long): Single<Result<Story>> {
        return apiClient.getItem(storyId)
                .subscribeOn(Schedulers.io())
                .map {
                    Story(it.id, it.author ?: "", it.time ?: 0L, it.title ?: "", it.url
                            ?: "", it.descendantsCount ?: 0)
                }
                .map { Result.value(it) }
                .onErrorResumeNext { error -> Single.just(Result.error(error)) }
    }
}