package me.grapescan.topnews.model.api

import io.reactivex.Single

/**
 * @author Dmitry Soldatov
 */
interface ApiClient {
    fun getTopStories(): Single<List<Long>>
    fun getItem(id: Long): Single<Item>
}