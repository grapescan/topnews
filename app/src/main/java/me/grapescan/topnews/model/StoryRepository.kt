package me.grapescan.topnews.model

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * @author Dmitry Soldatov
 */
interface StoryRepository {
    val stories: Observable<Result<List<Story>>>

    fun refreshStories(): Completable
    fun getComments(storyId: Long): Single<Result<List<Comment>>>
    fun getStory(storyId: Long): Single<Result<Story>>
}