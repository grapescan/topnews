package me.grapescan.topnews.model

/**
 * @author Dmitry Soldatov
 */
data class Result<out T>(val data: T?, val error: Throwable?) {
    companion object {

        @JvmStatic
        fun <T> value(data: T): Result<T> = Result(data, null)

        @JvmStatic
        fun <T> error(error: Throwable): Result<T> = Result(null, error)
    }
}