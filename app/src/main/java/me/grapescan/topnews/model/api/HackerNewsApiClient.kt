package me.grapescan.topnews.model.api

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import io.reactivex.Single
import me.grapescan.topnews.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.*

/**
 * @author Dmitry Soldatov
 */
class HackerNewsApiClient : ApiClient {

    override fun getTopStories(): Single<List<Long>> = apiService.getTopStories()

    override fun getItem(id: Long): Single<Item> = apiService.getItem(id)

    private val apiService = Retrofit.Builder()
            .baseUrl("https://hacker-news.firebaseio.com")
            .client(OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) {
                            HttpLoggingInterceptor.Level.BODY
                        } else {
                            HttpLoggingInterceptor.Level.NONE
                        }
                    })
                    .build())
            .addConverterFactory(GsonConverterFactory.create(
                    GsonBuilder()
                            .registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, typeOfT, context -> Date(json.asLong) })
                            .create()

            ))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ApiService::class.java)

    interface ApiService {

        @GET("/v0/topstories.json")
        fun getTopStories(): Single<List<Long>>

        @GET("/v0/item/{id}.json")
        fun getItem(@Path("id") id: Long): Single<Item>
    }
}