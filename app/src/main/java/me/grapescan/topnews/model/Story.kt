package me.grapescan.topnews.model

/**
 * @author Dmitry Soldatov
 */
data class Story(
        val id: Long,
        val author: String,
        val time: Long,
        val text: String,
        val link: String,
        val commentsCount: Int
) {
    val siteName: String
        get() = link.split("://")[1].split("/")[0] // TODO: handle corner cases
}
