package me.grapescan.topnews.model

/**
 * @author Dmitry Soldatov
 */
data class Comment(
        val id: Long,
        val level: Int,
        val author: String,
        val time: Long,
        val text: String,
        val commentIds: List<Long>
)