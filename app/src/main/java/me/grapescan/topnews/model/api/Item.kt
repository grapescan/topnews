package me.grapescan.topnews.model.api

import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Soldatov
 */
data class Item(
        @SerializedName("id") val id: Long = -1,
        @SerializedName("deleted") val isDeleted: Boolean?,
        @SerializedName("type") val type: Type?,
        @SerializedName("by") val author: String?,
        @SerializedName("time") val time: Long?,
        @SerializedName("text") val text: String?,
        @SerializedName("dead") val isDead: Boolean?,
        @SerializedName("parent") val parentId: Long?,
        @SerializedName("poll") val pollId: Long?,
        @SerializedName("kids") val commentIds: List<Long>?,
        @SerializedName("url") val url: String?,
        @SerializedName("score") val score: Long?,
        @SerializedName("title") val title: String?,
        @SerializedName("parts") val pollOptIds: List<Long>?,
        @SerializedName("descendants") val descendantsCount: Int?
) {
    enum class Type {
        JOB, STORY, COMMENT, POLL, POLLOPT
    }
}