package me.grapescan.topnews.ui.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_story_list.*
import me.grapescan.topnews.Injector
import me.grapescan.topnews.R
import me.grapescan.topnews.model.Story
import me.grapescan.topnews.ui.details.DetailsActivity

class StoryListActivity : AppCompatActivity(), StoryListView {

    private val presenter by lazy { Injector.newsListPresenter }
    private val listAdapter by lazy { StoryAdapter({ presenter.onStoryClick(it) }) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_list)
        setSupportActionBar(toolbar)
        list.adapter = listAdapter
        swipeContainer.setOnRefreshListener { presenter.onRefreshRequest() }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun showProgress() {
        swipeContainer.isRefreshing = true
    }

    override fun showStories(data: List<Story>) {
        swipeContainer.isRefreshing = false
        listAdapter.data = data
    }

    override fun openStoryDetails(storyId: Long) = DetailsActivity.start(this, storyId)

    override fun showError(error: Throwable) {
        swipeContainer.isRefreshing = false
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }
}
