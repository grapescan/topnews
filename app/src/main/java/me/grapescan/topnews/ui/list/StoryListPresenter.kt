package me.grapescan.topnews.ui.list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.topnews.model.StoryRepository
import me.grapescan.topnews.ui.base.Presenter

/**
 * @author Dmitry Soldatov
 */
class StoryListPresenter(private val storyRepository: StoryRepository) : Presenter<StoryListView>() {
    override fun attachView(viewParam: StoryListView) {
        super.attachView(viewParam)
        view?.showProgress()
        storyRepository.stories
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    when {
                        result.data != null -> view?.showStories(result.data)
                        result.error != null -> view?.showError(result.error)
                    }
                }, { view?.showError(it) })
                .disposeOnDetach()
        storyRepository.refreshStories().subscribe()
    }

    fun onStoryClick(storyId: Long) {
        view?.openStoryDetails(storyId)
    }

    fun onRefreshRequest() {
        view?.showProgress()
        storyRepository.refreshStories().subscribe()
    }
}