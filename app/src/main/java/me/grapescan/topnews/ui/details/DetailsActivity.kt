package me.grapescan.topnews.ui.details

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_details.*
import me.grapescan.topnews.Injector
import me.grapescan.topnews.R
import me.grapescan.topnews.model.Comment
import me.grapescan.topnews.model.Story

class DetailsActivity : AppCompatActivity(), DetailsView {

    companion object {

        private const val EXTRA_STORY_ID = "story_id"

        fun start(activity: Activity, storyId: Long) {
            val intent = Intent(activity, DetailsActivity::class.java)
            intent.putExtra(EXTRA_STORY_ID, storyId)
            activity.startActivity(intent)
        }
    }

    private val presenter by lazy { Injector.detailsPresenter.apply { storyId = intent.getLongExtra(EXTRA_STORY_ID, -1) } }
    private val listAdapter by lazy { CommentAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        headerBackButton.setOnClickListener { onBackPressed() }
        list.adapter = listAdapter
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun showError(error: Throwable) {
        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun showStory(story: Story) {
        headerTitle.text = story.text
        headerLink.text = story.siteName
        headerLink.setOnClickListener { openBrowser(story.link) }
        headerTitle.setOnClickListener { openBrowser(story.link) }
    }

    private fun openBrowser(link: String) = startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))

    override fun showComments(comments: List<Comment>) {
        listAdapter.data = comments
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

}
