package me.grapescan.topnews.ui.details

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.topnews.model.StoryRepository
import me.grapescan.topnews.ui.base.Presenter

class DetailsPresenter(private val storyRepository: StoryRepository) : Presenter<DetailsView>() {

    var storyId: Long = -1

    override fun attachView(viewParam: DetailsView) {
        super.attachView(viewParam)
        view?.showProgress()
        storyRepository.getStory(storyId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.data != null) {
                        view?.showStory(result.data)
                        loadComments()
                    } else if (result.error != null) {
                        view?.hideProgress()
                        view?.showError(result.error)
                    }
                }, { view?.showError(it) })
    }

    private fun loadComments() {
        storyRepository.getComments(storyId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.data != null) {
                        view?.hideProgress()
                        view?.showComments(result.data)
                    } else if (result.error != null) {
                        view?.hideProgress()
                        view?.showError(result.error)
                    }
                }, { view?.showError(it) })
    }

}
