package me.grapescan.topnews.ui.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_story.view.*
import me.grapescan.topnews.R
import me.grapescan.topnews.model.Story


/**
 * @author Dmitry Soldatov
 */
class StoryAdapter(private val clickListener: (Long) -> Unit) : RecyclerView.Adapter<StoryAdapter.StoryViewHolder>() {

    var data: List<Story> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged() // TODO: use DiffUtils
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_story, parent, false)
        return StoryViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: StoryViewHolder, position: Int) {
        holder.bind(data[position])
    }

    class StoryViewHolder(view: View, private val clickListener: (Long) -> Unit) : RecyclerView.ViewHolder(view) {

        private val title = itemView.storyTitle

        fun bind(story: Story) {
            title.text = story.text
            itemView.setOnClickListener { clickListener(story.id) }
        }
    }
}