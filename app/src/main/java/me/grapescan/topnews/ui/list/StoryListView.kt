package me.grapescan.topnews.ui.list

import me.grapescan.topnews.model.Story

/**
 * @author Dmitry Soldatov
 */
interface StoryListView {
    fun showProgress()
    fun showStories(data: List<Story>)
    fun showError(error: Throwable)
    fun openStoryDetails(storyId: Long)

}