package me.grapescan.topnews.ui.details

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.format.DateUtils
import android.text.format.DateUtils.FORMAT_ABBREV_RELATIVE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_comment.view.*
import me.grapescan.topnews.R
import me.grapescan.topnews.model.Comment

class CommentAdapter() : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {

    var data: List<Comment> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged() // TODO: use DiffUtils
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return CommentViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(data[position])
    }

    class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val title = itemView.commentTitle
        private val text = itemView.commentText

        fun bind(comment: Comment) {
            val timeString = DateUtils.getRelativeTimeSpanString(comment.time * 1000, System.currentTimeMillis(), 0, FORMAT_ABBREV_RELATIVE)
            title.text = "${comment.author} $timeString"
            text.text = Html.fromHtml(comment.text)
        }
    }
}