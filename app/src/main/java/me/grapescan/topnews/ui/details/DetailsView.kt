package me.grapescan.topnews.ui.details

import me.grapescan.topnews.model.Comment
import me.grapescan.topnews.model.Story

/**
 * @author Dmitry Soldatov
 */
interface DetailsView {
    fun showStory(story: Story)
    fun showError(error: Throwable)
    fun showComments(comments: List<Comment>)
    fun showProgress()
    fun hideProgress()

}