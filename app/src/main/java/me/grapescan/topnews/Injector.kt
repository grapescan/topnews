package me.grapescan.topnews

import me.grapescan.topnews.model.InMemoryStoryRepository
import me.grapescan.topnews.model.api.HackerNewsApiClient
import me.grapescan.topnews.ui.details.DetailsPresenter
import me.grapescan.topnews.ui.list.StoryListPresenter

object Injector {
    private val apiClient by lazy { HackerNewsApiClient() }
    private val storyRepository by lazy { InMemoryStoryRepository(apiClient) }
    val newsListPresenter by lazy { StoryListPresenter(storyRepository) }
    val detailsPresenter by lazy { DetailsPresenter(storyRepository) }
}