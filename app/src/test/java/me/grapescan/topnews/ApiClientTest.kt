package me.grapescan.topnews

import io.reactivex.observers.TestObserver
import me.grapescan.topnews.model.api.ApiClient
import me.grapescan.topnews.model.api.HackerNewsApiClient
import me.grapescan.topnews.model.api.Item
import org.junit.Before
import org.junit.Test

/**
 * @author Dmitry Soldatov
 */
class ApiClientTest {

    private lateinit var apiClient: ApiClient

    @Before
    fun setUp() {
        apiClient = HackerNewsApiClient()
    }

    @Test
    fun testStoriesCall() {
        val storyObserver = TestObserver<List<Long>>()
        apiClient.getTopStories()
                .subscribe(storyObserver)
        storyObserver.assertValueCount(1)
        storyObserver.assertValue { it.isNotEmpty() }
    }

    @Test
    fun testItemCall() {
        val itemObserver = TestObserver<Item>()
        apiClient.getItem(2921983)
                .subscribe(itemObserver)
        itemObserver.assertValueCount(1)
        itemObserver.assertValue { it.author == "norvig" }
    }
}