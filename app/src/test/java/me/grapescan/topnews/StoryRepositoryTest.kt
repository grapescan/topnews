package me.grapescan.topnews

import io.reactivex.observers.TestObserver
import me.grapescan.topnews.model.Comment
import me.grapescan.topnews.model.InMemoryStoryRepository
import me.grapescan.topnews.model.Story
import me.grapescan.topnews.model.StoryRepository
import me.grapescan.topnews.model.api.HackerNewsApiClient
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

/**
 * @author Dmitry Soldatov
 */
class StoryRepositoryTest {

    private lateinit var repo: StoryRepository

    @Before
    fun setUp() {
        repo = InMemoryStoryRepository(HackerNewsApiClient())
    }

    @Test
    fun testLoadStoriesContent() {
        val observer = TestObserver<List<Story>>()
        repo.refreshStories().subscribe(observer)
        observer.awaitDone(10, TimeUnit.SECONDS)
        repo.stories.subscribe(observer)
        observer.assertNoErrors()
        observer.awaitCount(3)
        observer.values()
    }

    @Test
    fun testLoadStoryComments() {
        val observer = TestObserver<List<Comment>>()
        repo.getComments(2921983).subscribe(observer)
        observer.awaitDone(10, TimeUnit.SECONDS)
        observer.assertValueCount(1)
        observer.assertValue { it.isNotEmpty() }
    }
}